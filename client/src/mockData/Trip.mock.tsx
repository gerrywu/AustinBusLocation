import { TripQuery } from "../schemas/Trip.generated";

export const tripQuery: TripQuery = {
  trip: {
    tripId: "2277200_MRG_6",
    tripHeadsign: "1-Lamar/South Congress SB",
    tripShortName: "William Cannon",
    routeId: "1",
    serviceId: "5-133_MRG_6",
    shapeId: "42692",
    wheelchairAccessible: 1,
    bikesAllowed: 1,
    __typename: "Trip",
  },
};
