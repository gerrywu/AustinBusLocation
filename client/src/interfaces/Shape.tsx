import { Shape } from "./interface.d";

export type ShapeData = Omit<Shape, "shapeId">;
